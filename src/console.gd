extends Control


onready var shell = $VBoxContainer/ScrollContainer/Label
onready var scroller = $VBoxContainer/ScrollContainer
onready var prompt = $VBoxContainer/HBoxContainer/TextEdit
onready var send_btn = $VBoxContainer/HBoxContainer/Button


# Called when the node enters the scene tree for the first time.
func _ready():
	shell.text = ""
	prompt.text = ""
	prompt.grab_focus()
	

func send_cmd():
	var cmd = prompt.text
	print(cmd)
	var output = []
	OS.execute('/system/bin/sh', ["-c", cmd], true, output )
	shell.text = cmd
	for o in output:
		shell.text += o
	shell.text += ""
	prompt.text = ""
	prompt.grab_focus()
	scroller.scroll_vertical = shell.get_line_count()

func _on_TextEdit_text_changed():
	var ns = prompt.cursor_get_line()
	if ns > 0:
		send_cmd()
